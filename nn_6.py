import matplotlib.pyplot as plt
import cifar10
import numpy as np
import math
from keras.layers import Input, Reshape, MaxPooling2D, Conv2D, Dense, Flatten, Lambda
from keras.models import Model
from cifar10 import img_size, num_channels, num_classes
import keras.backend as K
import tensorflow as tf
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
path_cp = 'model/nn_6/cp/cp.hdf5'
callbacks = [
    EarlyStopping(monitor='val_loss', patience=2, verbose=1, restore_best_weights=True),
    ModelCheckpoint(path_cp, monitor='val_loss', save_best_only=True, verbose=1, period=1)
]

cifar10.data_path = "data/cifar10/"
cifar10.maybe_download_and_extract()
class_names = cifar10.load_class_names()
images_train, cls_train, labels_train = cifar10.load_training_data()
images_test, cls_test, labels_test = cifar10.load_test_data()

img_size_cropped = 24


def plot_images(images, cls_true, cls_pred=None, smooth=True):
    assert len(images) == len(cls_true) == 9

    # Create figure with 3x3 sub-plots.
    fig, axes = plt.subplots(3, 3)
    fig.subplots_adjust(hspace=0.3, wspace=0.3)

    for i, ax in enumerate(axes.flat):
        if smooth:
            interpolation = 'spline16'
        else:
            interpolation = 'nearest'

        # Plot image.
        ax.imshow(images[i, :, :, :], interpolation=interpolation)

        cls_true_name = class_names[cls_true[i]]

        # Show true and predicted classes.
        if cls_pred is None:
            xlabel = "True: {0}".format(cls_true_name)
        else:
            cls_pred_name = class_names[cls_pred[i]]
            xlabel = "True: {0}, Pred: {1}".format(cls_true_name, cls_pred_name)

        # Show the classes as the label on the x-axis.
        ax.set_xlabel(xlabel)

        # Remove ticks from the plot.
        ax.set_xticks([])
        ax.set_yticks([])

    # Ensure the plot is shown correctly with multiple plots
    # in a single Notebook cell.
    plt.show()


# images = images_test[0:9]
# cls_true = cls_test[0:9]
# plot_images(images=images, cls_true=cls_true, smooth=True)


def pre_process_image(image, training):
    def train(image):
        image = tf.random_crop(image, size=[img_size_cropped, img_size_cropped, num_channels])
        image = tf.image.random_flip_left_right(image)
        image = tf.image.random_hue(image, max_delta=0.05)
        image = tf.image.random_contrast(image, lower=0.3, upper=1.0)
        image = tf.image.random_brightness(image, max_delta=0.2)
        image = tf.image.random_saturation(image, lower=0.0, upper=2.0)
        # Some of these functions may overflow and result in pixel
        # values beyond the [0, 1] range. It is unclear from the
        # documentation of TensorFlow 0.10.0rc0 whether this is
        # intended. A simple solution is to limit the range.

        # Limit the image pixels between [0, 1] in case of overflow.
        image = tf.minimum(image, 1.0)
        image = tf.maximum(image, 0.0)
        return image

    def test(image):
        # For training, add the following to the TensorFlow graph.

        # Crop the input image around the centre so it is the same
        # size as images that are randomly cropped during training.
        return tf.image.resize_image_with_crop_or_pad(image, target_height=img_size_cropped, target_width=img_size_cropped)

    return tf.cond(tf.equal(training, tf.constant(1., shape=())), lambda: train(image), lambda: test(image))


def pre_process(args):
    # Use TensorFlow to loop over all the input images and call
    # the function above which takes a single image as input.
    images = args[0]
    trainings = args[1]
    return tf.map_fn(lambda x: pre_process_image(x[0], x[1]), (images, trainings), dtype=tf.float32)


inputs_images = Input(shape=(img_size, img_size, num_channels))
inputs_trainings = Input(shape=())
pre_processed_images = Lambda(pre_process)([inputs_images, inputs_trainings])
conv1 = Conv2D(kernel_size=5, strides=1, filters=64, padding='same', activation='relu')(pre_processed_images)
pool1 = MaxPooling2D(pool_size=2, strides=2)(conv1)
conv2 = Conv2D(kernel_size=5, strides=1, filters=64, padding='same', activation='relu')(pool1)
pool2 = MaxPooling2D(pool_size=2, strides=2)(conv2)
flattened = Flatten()(pool2)
dense1 = Dense(256, activation='relu')(flattened)
dense2 = Dense(128, activation='relu')(dense1)
output = Dense(num_classes, activation='softmax')(dense2)

kf_pre_process = K.function(inputs=[inputs_images, inputs_trainings], outputs=[pre_processed_images])
# image1 = images_test[0]
# image1_after_pre_process = kf_pre_process([[image1], [1.]])[0][0]
# plt.imshow(image1_after_pre_process, interpolation='spline16')
# plt.show()

model = Model(inputs=[inputs_images, inputs_trainings], outputs=output)
optimizer = Adam(lr=1e-4)
model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

model.fit(x=[images_train[:40000], np.full((40000,), 1., dtype=np.float32)], y=labels_train[:40000], epochs=100, batch_size=64, verbose=2, callbacks=callbacks, validation_data=([images_train[40000:], np.full((10000,), 0., dtype=np.float32)], labels_train[40000:]), shuffle=True)
result = model.evaluate(x=[images_test, np.full((10000,), 0., dtype=np.float32)], y=labels_test)
for name, value in zip(model.metrics_names, result):
    print(name, value)

path_model = 'model/nn_6/model.keras'
model.save(path_model)

