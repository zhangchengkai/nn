import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import math

from keras.layers import Input, Reshape, MaxPooling2D, Conv2D, Dense, Flatten

from mnist import MNIST
data = MNIST(data_dir="data/")

# The number of pixels in each dimension of an image.
img_size = data.img_size

# The images are stored in one-dimensional arrays of this length.
img_size_flat = data.img_size_flat

# Tuple with height and width of images used to reshape arrays.
img_shape = data.img_shape

# Tuple with height, width and depth used to reshape arrays.
# This is used for reshaping in Keras.
img_shape_full = data.img_shape_full

# Number of classes, one class for each of 10 digits.
num_classes = data.num_classes

# Number of colour channels for the images: 1 channel for gray-scale.
num_channels = data.num_channels


def plot_images(images, cls_true, cls_pred=None):
    assert len(images) == len(cls_true) == 9

    # Create figure with 3x3 sub-plots.
    fig, axes = plt.subplots(3, 3)
    fig.subplots_adjust(hspace=0.3, wspace=0.3)

    for i, ax in enumerate(axes.flat):
        # Plot image.
        ax.imshow(images[i].reshape(img_shape), cmap='binary')

        # Show true and predicted classes.
        if cls_pred is None:
            xlabel = "True: {0}".format(cls_true[i])
        else:
            xlabel = "True: {0}, Pred: {1}".format(cls_true[i], cls_pred[i])

        # Show the classes as the label on the x-axis.
        ax.set_xlabel(xlabel)

        # Remove ticks from the plot.
        ax.set_xticks([])
        ax.set_yticks([])

    # Ensure the plot is shown correctly with multiple plots
    # in a single Notebook cell.
    plt.show()


def plot_example_errors(cls_pred):
    incorrect = (cls_pred != data.y_test_cls)
    images = data.x_test[incorrect]
    cls_pred = cls_pred[incorrect]
    cls_true = data.y_test_cls[incorrect]
    plot_images(images=images[0:9], cls_true=cls_true[0:9], cls_pred=cls_pred[0:9])


inputs = Input(shape=(img_size_flat,))
net = inputs
net = Reshape(img_shape_full)(net)
net = Conv2D(kernel_size=5, strides=1, filters=16, padding='same', activation='relu', name='layer_conv1')(net)
net = MaxPooling2D(pool_size=2, strides=2)(net)
net = Conv2D(kernel_size=5, strides=1, filters=36, padding='same', activation='relu', name='layer_conv2')(net)
net = MaxPooling2D(pool_size=2, strides=2)(net)
net = Flatten()(net)
net = Dense(128, activation='relu')(net)
net = Dense(num_classes, activation='softmax')(net)
outputs = net

from keras.models import Model
model = Model(inputs=inputs, outputs=outputs)
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

from keras.callbacks import EarlyStopping, ModelCheckpoint
path_cp = 'model/nn_3c_func/cp/cp.hdf5'
callbacks = [
    EarlyStopping(monitor='val_loss', patience=2, verbose=1, restore_best_weights=True),
    ModelCheckpoint(path_cp, monitor='val_loss', save_best_only=True, verbose=1, period=1)
]

model.fit(x=data.x_train, y=data.y_train, epochs=100, batch_size=128, verbose=2, callbacks=callbacks, validation_data=(data.x_val, data.y_val), shuffle=True)

result = model.evaluate(x=data.x_test, y=data.y_test)
for name, value in zip(model.metrics_names, result):
    print(name, value)

y_pred = model.predict(x=data.x_test)
cls_pred = np.argmax(y_pred, axis=1)
plot_example_errors(cls_pred)

path_model = 'model/nn_3c_func/model.keras'
model.save(path_model)
del model

from keras.models import load_model
model_loaded = load_model(path_model)

images = data.x_test[0:9]
cls_true = data.y_test_cls[0:9]
y_pred = model_loaded.predict(x=images)
cls_pred = np.argmax(y_pred, axis=1)
plot_images(images=images, cls_pred=cls_pred, cls_true=cls_true)


def plot_conv_weights(weights, input_channel):
    w_min = np.min(weights)
    w_max = np.max(weights)
    num_filters = weights.shape[3]
    num_grids = math.ceil(math.sqrt(num_filters))
    fig, axes = plt.subplots(num_grids, num_grids)
    for i, ax in enumerate(axes.flat):
        if i<num_filters:
            img = weights[:,:,input_channel,i]
            ax.imshow(img, vmin=w_min, vmax=w_max, interpolation='nearest', cmap="seismic")
        ax.set_xticks([])
        ax.set_yticks([])
    plt.show()


model_loaded.summary()
layer_conv1 = model_loaded.layers[2]
layer_conv2 = model_loaded.layers[4]
weights_conv1 = layer_conv1.get_weights()[0]
plot_conv_weights(weights=weights_conv1, input_channel=0)
weights_conv2 = layer_conv2.get_weights()[0]
plot_conv_weights(weights=weights_conv2, input_channel=0)


def plot_conv_layer(values):
    num_filters = values.shape[3]
    num_grids = math.ceil(math.sqrt(num_filters))
    fig, axes = plt.subplots(num_grids, num_grids)
    for i, ax in enumerate(axes.flat):
        if i < num_filters:
            img = values[0, :, :, i]
            ax.imshow(img, interpolation='nearest', cmap="binary")
        ax.set_xticks([])
        ax.set_yticks([])
    plt.show()


def plot_image(image):
    plt.imshow(image.reshape(img_shape), interpolation='nearest', cmap="binary")
    plt.show()


image1 = data.x_test[0]
plot_image(image1)

from keras import backend as K

layer_input = model_loaded.layers[0]
output_conv1 = K.function(inputs=[layer_input.input], outputs=[layer_conv1.output])
layer_output1 = output_conv1([[image1]])[0]
plot_conv_layer(values=layer_output1)
output_conv2 = Model(inputs=layer_input.input, outputs=layer_conv2.output)
layer_output2 = output_conv2.predict(np.array([image1]))
plot_conv_layer(values=layer_output2)

